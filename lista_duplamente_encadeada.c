#include <stdio.h>
#include <stdlib.h>

 typedef struct no{
	int info;
	struct no *ant;
	struct no *prox;
}*ptno;

void insere(ptno *L, int valor){
	ptno aux = (ptno) malloc (sizeof(struct no));
	if(!aux)
		puts("Memória cheia!");
	else{
		ptno P = NULL, Q = *L;
		while(Q && Q->info <= valor){
			P = Q;
			Q = Q->prox;
		}
		aux->info = valor;
		aux->prox = Q; 
		aux->ant = P;
		if(!P)
			*L = aux;
		else
			P->prox = aux;
		if(Q)
			Q->ant = aux;
	}
}

void retira(ptno *L, int valor){
	if(!(*L))
		puts("Lista Vazia !");
	else{
		ptno aux = *L, P=NULL, Q;
		while(aux && aux->info < valor){
			P = aux;
			aux = aux->prox;
		}
		if(!aux || aux->info > valor)
			puts("Valor não encontrado !");
		else{
			Q = aux->prox;
			if(!P)
				*L = Q;
			else
				P->prox = Q;
			if(Q)
				Q->ant = P;
			free(aux);
		}
	}
}

void mostra(ptno L){
	printf("{");
	while(L){
		printf("%d ", L->info);
		ptno aux = L;
		L = aux->prox;
	}
	printf("}\n");
}

void main(){
	ptno L;
	insere(&L, 10);
	insere(&L, 20);
	insere(&L, 30);
	mostra(L);
	retira(&L, 10);
	mostra(L);
	retira(&L, 30);
	mostra(L);
}
